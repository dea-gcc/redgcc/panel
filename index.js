#!/usr/bin/env node
const Client = require('ssb-client')
const Config = require('ssb-config/inject')
const ref = require('ssb-ref')
const neatLog = require('neat-log')
const out = require('neat-log/output')
const c = require('ansi-colors')
const prettyHash = require('pretty-hash')

const opts = {
  caps: require('@redgcc/semilla'),
  port: 9999
}

const NAME = 'redgcc'
const config = Config(NAME, opts)

const initState = {
  id: '',
  name: '',
  shs: config.caps.shs,
  localPeers: [],
  path: ''
}

const app = neatLog(view, { fullscreen: true, state: initState })
function view (state) {
  return out(`
  ${c.red('saludo secreto:')} ${prettyHash(state.shs)}
  ${c.red('ruta a la data:')} ${state.path}
  ${c.red('id:')} ${c.green(state.name)} (${prettyHash(state.id)})
  ${c.red('n peers locales:')} ${state.localPeers.length} (${getConnectedPeers()})
  ${c.green('lista de peers:')}
  ${getPeersKeyList()}
  `
  )

  function getConnectedPeers () {
    return state.localPeers.filter(peer => peer.connected).length
  }
  function getPeersKeyList () {
    return state.localPeers.map(peer => {
      return `${c.magenta(peer.name)}(${prettyHash(ref.getKeyFromAddress(peer.address))})(${c.green(peer.following ? '*' : '')})`
    }).join('\n')
  }
}

app.use((state, emitter) => {
  // Intenta conectarte cada 1 seg
  var interval = setInterval(tryConnect, 1000)

  function tryConnect () {
    Client(config.keys, config, (err, sbot) => {
      if (err) {
        if (/could not connect/.test(err.message)) {
          console.log('server no disponible...')
          return
        } else {
          throw err
        }
      }
      // Si ya me pude conectar dejá de intentar
      clearInterval(interval)
      state.id = sbot.id
      state.path = config.path
      emitter.emit('sbot:up', sbot)
      emitter.emit('render')
    })
  }
})

app.use(require('./checkLocalPeers.js'))
app.use(require('./checkIfFollowing.js'))
app.use(require('./getName.js'))
