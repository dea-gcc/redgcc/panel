const pull = require('pull-stream')

module.exports = function (state, emitter) {
  var sbot = null

  emitter.on('sbot:up', (server) => {
    if (!server) throw new Error('pasale un server, watch')
    if (!server.query) throw new Error('necesito el plugin ssb-query instalado :/')
    sbot = server
  })

  emitter.on('newPeer', (idx) => {
    const peerId = state.localPeers[idx].key
    getFollowing(sbot, state.id, peerId, (err, following) => {
      if (err) throw err

      if (following[0]) {
        state.localPeers[idx].following = following
      } else {
        state.localPeers[idx].following = false
      }
      emitter.emit('render')
    })
  })
}

function getFollowing (sbot, usrId, peerId, cb) {
  const opts = {
    limit: 1,
    reverse: true,
    query: [
      {
        $filter: {
          value: {
            author: usrId,
            content: {
              type: 'contact',
              contact: peerId
            }
          }
        }
      },
      {
        $map: ['value', 'content', 'following']
      }
    ]
  }
  pull(
    sbot.query.read(opts),
    pull.collect(checkMessages)
  )

  function checkMessages (err, res) {
    if (err) return cb(err)
    // console.log(`RESUPUESTA ${res['0']}`)
    cb(null, res)
  }
}
