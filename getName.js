const pull = require('pull-stream')

module.exports = function (state, emitter) {
  var sbot = null
  emitter.on('sbot:up', (server) => {
    if (!server) throw new Error('pasale un server, watch')
    if (!server.query) throw new Error('necesito el plugin ssb-query instalado :/')
    sbot = server
    getAbout(sbot, state.id, (err, name) => {
      if (err) throw err
      if (!name) {
        state.name = 'anon'
      } else {
        state.name = name
      }
      emitter.emit('render')
    })
  })

  emitter.on('newPeer', idx => {
    getAbout(sbot, state.localPeers[idx].key, (err, name) => {
      if (err) throw err
      if (!name) {
        state.localPeers[idx].name = 'anon'
      } else {
        state.localPeers[idx].name = name
      }
      emitter.emit('render')
    })
  })
}

function getAbout (sbot, id, cb) {
  // QUERY
  const opts = {
    limit: 1,
    reverse: true, // ultimo mensaje de about
    query: [
      {
        $filter: {
          // Es un mensaje de about del author sobre si mismo
          value: {
            author: id,
            content: {
              type: 'about',
              about: id,
              name: { $is: 'string' }
            }
          },
          timestamp: { $gt: 0 } // para ordenar por timestamp
        }
      },
      {
        $map: {
          name: ['value', 'content', 'name']
        }
      }
    ]
  }

  pull(
    sbot.query.read(opts),
    pull.collect(checkMessages)
  )

  function checkMessages (err, results) {
    if (err) return cb(err)

    if (!results || !results.length) {
      return cb(null, null)
    } else {
      return cb(null, results[0].name)
    }
  }
}
