const pull = require('pull-stream')
const throttle = require('pull-throttle')
const ref = require('ssb-ref')

// Descuber y conecta un peer en la red
module.exports = function (state, emitter) {
  emitter.on('sbot:up', function (sbot) {
    if (!sbot) throw new Error('pasale un server, watch')
    if (!sbot.lan) throw new Error('necesito el plugin ssb-lan instalado :/')
    // empezá a buscar peers en la red local
    sbot.lan.start()

    pull(
      sbot.lan.discoveredPeers(),
      throttle(1000),
      pull.drain(onPeer)
    )

    // Cada vez que me fijo las conexiones
    function onPeer ({ address }) {
      var alreadyKnown = false

      for (let i = 0; i < state.localPeers.length; i++) {
        var obj = state.localPeers[i]
        // Si el peer ya existe
        if (obj.address === address) {
          state.localPeers[i].checker()
          alreadyKnown = true
          break
        }
      }

      if (!alreadyKnown) {
        addPeer(address)
      }

      // Si es el primer peer
      if (state.localPeers.length === 0) {
        addPeer(address)
      }
    }

    function addPeer (address) {
      const peer = {
        address: address,
        connected: false,
        key: ref.getKeyFromAddress(address)
      }
      // Agrega a la lista de peers descubiertos
      state.localPeers.push(peer)
      const newPeerIdx = state.localPeers.length - 1
      state.localPeers[newPeerIdx].checker = checker(newPeerIdx)

      // Conectate
      sbot.conn.connect(address, { type: 'lan', key: peer.key }, (err, d) => {
        if (err) throw err
        state.localPeers[newPeerIdx].connected = true
        emitter.emit('render')
      })
      emitter.emit('newPeer', newPeerIdx)
      emitter.emit('render')
    }

    // Cada 1 segundo sube un contador, si llega a 10(segundos) el peer se desconectó
    function checker (idx) {
      var count = 0
      var interval = setInterval(function () {
        if (count === 10) {
          state.localPeers.splice(idx, 1)
          clearInterval(interval)
          emitter.emit('render')
        }
        count++
      }, 1000)
      return function () {
        count = 0
      }
    }
  })
}
